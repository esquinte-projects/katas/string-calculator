package esquinte.projects.katas;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class StringCalculatorTest {
    private StringCalculator calculator;

    @BeforeEach
    public void init() {
        calculator = new StringCalculator();
    }

    // Step 1
    @Test
    public void shouldReturnZeroWhenNull() {
        int result = calculator.add(null);
        assertThat(result).isZero();
    }

    @Test
    public void shouldReturnZeroWhenEmpty() {
        int result = calculator.add("");
        assertThat(result).isZero();
    }

    @Test
    public void shouldReturnZeroWhenWhiteSpace() {
        int result = calculator.add(" ");
        assertThat(result).isZero();

        result = calculator.add("1,2 ");
        assertThat(result).isZero();

        result = calculator.add("1, 2");
        assertThat(result).isZero();

        result = calculator.add(" 1,2");
        assertThat(result).isZero();

        result = calculator.add("1 ,2");
        assertThat(result).isZero();
    }

    @Test
    public void shouldReturnZeroWhenInvalidInput() {
        int result = calculator.add(",");
        assertThat(result).isZero();

        result = calculator.add(",1,2");
        assertThat(result).isZero();

        result = calculator.add("1,2,");
        assertThat(result).isZero();

        result = calculator.add("1,,2");
        assertThat(result).isZero();
    }

    @Test
    public void shouldSumNumbers() {
        int result = calculator.add("0");
        assertThat(result).isEqualTo(0);

        result = calculator.add("24");
        assertThat(result).isEqualTo(24);

        result = calculator.add("1,2");
        assertThat(result).isEqualTo(3);
    }

    // Step 2
    @Test()
    public void shouldSumUnknownAmountOfNumbers() {
        int result = calculator.add("1,2,3,4,5,6,7,8,9");
        assertThat(result).isEqualTo(45);

        result = calculator.add("11,2,3,57,44,156,2");
        assertThat(result).isEqualTo(275);
    }

    // Step 3
    @Test()
    public void shouldHandleNewLinesSeparator() {
        int result = calculator.add("\n");
        assertThat(result).isEqualTo(0);

        result = calculator.add(",\n");
        assertThat(result).isEqualTo(0);

        result = calculator.add("\n,");
        assertThat(result).isEqualTo(0);

        result = calculator.add("1\n2,3");
        assertThat(result).isEqualTo(6);

        result = calculator.add("1\n2\n3\n4");
        assertThat(result).isEqualTo(10);

    }

    // Step 4
    @Test()
    public void shouldSupportDifferentDelimiters() {
        int result = calculator.add("//\n1\n2");
        assertThat(result).isEqualTo(0);

        result = calculator.add("//");
        assertThat(result).isEqualTo(0);

        result = calculator.add("//1,2");
        assertThat(result).isEqualTo(0);

        result = calculator.add("//;\n1,2");
        assertThat(result).isEqualTo(0);

        result = calculator.add("//-\n1-2-3-4-5");
        assertThat(result).isEqualTo(15);

        result = calculator.add("//aa\n1aa2aa3aa4aa5");
        assertThat(result).isEqualTo(15);

        result = calculator.add("//eee\n1eee2\n3\n4eee10");
        assertThat(result).isEqualTo(20);

        result = calculator.add("//i\n1\n2\n3\n4");
        assertThat(result).isEqualTo(10);
    }

    // Step 5
    @Test()
    public void shouldThrowExceptionWhenNegativesNumbers() {
        assertThatThrownBy(() -> {
            calculator.add("-1");
        }).isInstanceOf(IllegalArgumentException.class).hasMessage("negatives not allowed : [-1]");

        assertThatThrownBy(() -> {
            calculator.add("-1,-3,-5");
        }).isInstanceOf(IllegalArgumentException.class).hasMessage("negatives not allowed : [-1, -3, -5]");

        assertThatThrownBy(() -> {
            calculator.add("1,-3,5");
        }).isInstanceOf(IllegalArgumentException.class).hasMessage("negatives not allowed : [-3]");
    }
}
