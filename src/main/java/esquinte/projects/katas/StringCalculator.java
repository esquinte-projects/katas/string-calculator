package esquinte.projects.katas;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class StringCalculator {
    private static final String NEW_LINE = "\n";
    private static final String COMMA = ",";

    /**
     * Somme les nombres contenus dans la chaîne de caractères en entrée
     * 
     * @param numbers la chaîne de caractères en entrée
     * @return la somme des nombres
     */
    public int add(String numbers) {
        if (!isInputValid(numbers)) {
            return 0;
        }

        Map<Boolean, List<Integer>> map = extractNumber(numbers);

        List<Integer> negativeNumbers = map.get(Boolean.FALSE);
        if (!negativeNumbers.isEmpty()) {
            throw new IllegalArgumentException("negatives not allowed : " + negativeNumbers);
        }

        return map.get(Boolean.TRUE).stream().mapToInt(Integer::intValue).sum();
    }

    /**
     * Détermine si la chaîne de caractères est valide
     * 
     * @param numbers la chaîne de caractères en entrée
     * @return true si la chaîne de caractères est valide, sinon false
     */
    private boolean isInputValid(String numbers) {
        if (numbers == null) {
            return false;
        }

        String delimiter = getDelimiter(numbers);

        // Regex vérifiant si la chaîne de caractères :
        // - commence par "//{delimiter}\n" ou par un nombre
        // - ne comporte uniquement des nombres, sauts de ligne, chaînes de caractères
        // représentant le {delimiter} ou à defaut des virgules
        // - se termine par un nombre
        return numbers.matches("^(\\/\\/.*\n.*)?-?\\d+$|^(\\/\\/.*\n.*)?(-?\\d+(" + delimiter + "))+-?\\d+$");
    }

    /**
     * Récupère le séparateur utilisé pour sommer les nombres
     * 
     * @param numbers la chaîne de caractères en entrée
     * @return le séparateur
     */
    private String getDelimiter(String numbers) {
        String delimiter = COMMA;

        if (numbers.startsWith("//")) {
            int endIndex = numbers.indexOf("\n");
            if (endIndex == -1) {
                return null;
            }
            delimiter = numbers.substring(2, endIndex);
            if (delimiter.isEmpty()) {
                return null;
            }
        }

        return NEW_LINE + "|" + delimiter;
    }

    /**
     * Récupère les nombres positifs et négatifs
     * 
     * @param numbers la chaîne de caractères en entrée
     * @return une map contenant les listes des nombres positifs et négatifs
     */
    private Map<Boolean, List<Integer>> extractNumber(String numbers) {
        String delimiter = getDelimiter(numbers);

        return Arrays.stream(numbers.split(delimiter)).filter(this::isInteger).map(Integer::valueOf)
                .collect(Collectors.partitioningBy(this::isPositiveInteger));

    }

    /**
     * Détermine si la chaîne de caractères est un entier
     * 
     * @param number la chaîne de caractère à tester
     * @return true si la chaîne de caractère est un entier, sinon false
     */
    private boolean isInteger(String number) {
        return number.matches("-?\\d+");
    }

    /**
     * Détermine si l'entier est un entier naturel
     * 
     * @param number l'entier à tester
     * @return true si l'entier est un entier naturel, sinon false
     */
    private boolean isPositiveInteger(Integer number) {
        return number >= 0;
    }
}
